# Builder
FROM node:latest AS builder
WORKDIR /app
COPY . .
#RUN npm config set legacy-peer-deps true
RUN npm install
RUN npm run build
# EXPOSE port
CMD [ "node", "dist/app.js" ]
