# Recaptcha Server

## Installing
just run
```bash
npm i
```
to install deps of the server

## Running dev server
```
npm run dev
```

## Running in production
```
npm start
```
