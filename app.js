const express = require("express");
const node_fetch = require("node-fetch");
const cors = require("cors");
require("dotenv").config();

const app = express();
const PORT = process.env.PORT || 2000;

app.use(express.json());
app.options('*', cors());
app.use(cors());

app.post("/management-dashboard", async (req, res) => {
  const { token } = req.body;

  try {
    const response = await node_fetch(
      `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.NTX_KEY}&response=${token}`,
      {
        method: "POST",
      }
    );

    const data = await response.json();

    if (data.success) {
      // reCAPTCHA verification successful
      res.json({ success: true });
    } else {
      // reCAPTCHA verification failed
      res.json({ success: false });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal server error");
  }
});

app.post("/compute-request", async (req, res) => {
  const { token } = req.body;

  try {
    const response = await node_fetch(
      `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.COMPUTE_KEY}&response=${token}`,
      {
        method: "POST",
      }
    );

    const data = await response.json();

    if (data.success) {
      // reCAPTCHA verification successful
      res.json({ success: true });
    } else {
      // reCAPTCHA verification failed
      res.json({ success: false });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal server error");
  }
});

app.get("/status", (req, res) => {
  res.status(200).json({ running: true });
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
